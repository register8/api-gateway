package api

import (
	v1 "temp/api/handlers/v1"
	"temp/config"
	"temp/pkg/logger"
	"temp/services"

	"github.com/gin-gonic/gin"
)

type Option struct{
	Conf config.Config
	Logger logger.Logger
	ServiceManager services.IServiceManager
}

func New(option Option) *gin.Engine{
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.Handlerv1Config{
		Logger: option.Logger,
		ServiceManager: option.ServiceManager,
		CFG: option.Conf,
	})
	api := router.Group("/v1")

	api.POST("/users", handlerV1.CreateUser)
	api.GET("/users/get/:id", handlerV1.GetUser)
	api.PUT("/users/update", handlerV1.UpdateUser)
	api.DELETE("/users/delete/:id", handlerV1.DeleteUser)

	return router

}