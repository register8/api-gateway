package v1

import (
	"context"
	"strconv"

	"net/http"
	"temp/genproto/user"
	"temp/pkg/logger"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body user.User
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error":err.Error(),
		})
		h.log.Error("Failed bind JSON",logger.Error(err))
	}
	ctx, cancel := context.WithTimeout(context.Background(),time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	responce, err := h.servicemanager.UserService().CreateUser(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error":err.Error(),
		})
		h.log.Error("Failed THE bind JSON",logger.Error(err))
		return
	}
	c.JSON(http.StatusCreated, responce)
}
func (h *handlerV1) GetUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.servicemanager.UserService().GetUser(ctx, &user.UserID{UserId: id})
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}
func (h *handlerV1) UpdateUser(c *gin.Context) {
	var (
		body user.User
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(),time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.servicemanager.UserService().UpdateUser(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to Update user", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}
func (h *handlerV1) DeleteUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(),time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.servicemanager.UserService().DeleteUser(ctx, &user.UserID{UserId: id})
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}
