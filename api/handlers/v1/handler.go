package v1	
import (
	"temp/config"
	"temp/pkg/logger"
	"temp/services"
)

type handlerV1 struct{
	log logger.Logger
	servicemanager services.IServiceManager
	cfg config.Config
}
type Handlerv1Config struct{
	Logger logger.Logger
	ServiceManager services.IServiceManager
	CFG config.Config
}

func New(c *Handlerv1Config) *handlerV1{
	return &handlerV1{
		log: c.Logger,
		servicemanager: c.ServiceManager,
		cfg: c.CFG,
	}
}