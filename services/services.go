package services

import (
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"

	"temp/config"
	pb "temp/genproto/product"
	ps "temp/genproto/user"
)

type IServiceManager interface{
	UserService() ps.UserServiceClient
	ProductService() pb.ProductServiceClient
}

type serviceManager struct {
	userService ps.UserServiceClient
	productService pb.ProductServiceClient
}

func (s *serviceManager) UserService() ps.UserServiceClient{
	return s.userService
}
func (p *serviceManager) ProductService() pb.ProductServiceClient{
	return p.productService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d",conf.UserServiceTempHost, conf.UserServiceTempPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil{
		return nil, err
	}
	connProduct, err := grpc.Dial(
		fmt.Sprintf("%s:%d",conf.ProductServiceHost,conf.ProductServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil{
		return nil, err
	}

	serviceManage := &serviceManager{
		userService: ps.NewUserServiceClient(connUser),
		productService: pb.NewProductServiceClient(connProduct),
	}
	return serviceManage, nil
}